// src/mocks/handlers.js
import { rest } from "msw";
import { API_ORIGIN } from "../actions";

export const handlers = [
  rest.get(API_ORIGIN + "/api/categories", (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: 1,
          name: "Kitchen",
        },
      ])
    );
  }),
  rest.get(API_ORIGIN + "/api/products", (req, res, ctx) => {
    return res(
      ctx.json({
        content: [],
        pageable: {
          sort: {
            sorted: false,
            unsorted: true,
            empty: true,
          },
          offset: 0,
          pageNumber: 0,
          pageSize: 1,
          paged: true,
          unpaged: false,
        },
        last: false,
        totalPages: 34,
        totalElements: 34,
        size: 1,
        number: 0,
        sort: {
          sorted: false,
          unsorted: true,
          empty: true,
        },
        numberOfElements: 1,
        first: true,
        empty: false,
      })
    );
  }),
];
