import { actionType } from "../actions";

const initialState = {
  products: {
    content: [],
    pageable: {
      number: 0,
      totalPages: 1,
    },
  },
  allCategories: null,
  categories: [],
  lastQuery: {
    page: 0,
    size: 5,
    sort: "name,asc",
  },
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_PRODUCTS:
      return {
        ...state,
        lastQuery: {
          ...state.lastQuery,
          ...action.query,
        },
        products: action.payload,
        categories: countCategories(
          state.allCategories,
          action.payload.content
        ),
      };
    case actionType.GET_CATEGORIES:
      const allCategories = {};
      for (const category of action.payload) {
        allCategories[category.id] = category;
      }
      return {
        ...state,
        allCategories,
        categories: countCategories(allCategories, state.products),
      };
    case actionType.FILTER_PRODUCTS:
      return {
        ...state,
        products: state.products.filter((item) => {
          return action.payload ? action.payload === item.category : item;
        }),
        categories: state.categories,
      };
    case actionType.GET_PRODUCTS_FAILURE:
    case actionType.GET_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default reducer;

function countCategories(allCategories, itemArray) {
  if (!allCategories) return;
  let hasSeen = {},
    itemArrayLength = itemArray.length;

  for (let i = 0; i < itemArrayLength; i++) {
    const categoryId = itemArray[i].categoryId;
    hasSeen[categoryId]
      ? (hasSeen[categoryId] += 1)
      : (hasSeen[categoryId] = 1);
  }

  return Object.keys(allCategories)
    .map((categoryId) => {
      return {
        name: allCategories[categoryId].name,
        count: hasSeen[categoryId] || 0,
      };
    })
    .sort(function (a, b) {
      return b.count - a.count;
    });
}
