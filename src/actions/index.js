const fetch = require("node-fetch");

export const API_ORIGIN = process.env.API_URL || "http://localhost:8080";
const GET_PRODUCTS_URI = API_ORIGIN + "/api/products";
const GET_CATEGORIES_URI = API_ORIGIN + "/api/categories";

export const actionType = {
  GET_PRODUCTS: "get_products",
  GET_CATEGORIES: "get_categories",
  FILTER_PRODUCTS: "filter_products",
  GET_PRODUCTS_FAILURE: "get_products_failure",
  GET_CATEGORIES_FAILURE: "get_categories_failure",
};

export function getCategories() {
  return async (dispatch) => {
    try {
      const response = await fetch(GET_CATEGORIES_URI);
      const payload = await response.json();
      dispatch({
        type: actionType.GET_CATEGORIES,
        payload,
      });
    } catch (e) {
      dispatch({
        type: actionType.GET_CATEGORIES_FAILURE,
        error: new Error("Fail to get categories"),
      });
    }
  };
}

export function getProducts(params) {
  return async (dispatch) => {
    try {
      const url = new URL(GET_PRODUCTS_URI);
      Object.keys(params || {}).forEach((key) =>
        url.searchParams.append(key, params[key])
      );
      const response = await fetch(url);
      const payload = await response.json();
      dispatch({
        type: actionType.GET_PRODUCTS,
        payload,
        query: {
          number: payload.number,
          size: payload.size,
        },
      });
    } catch (e) {
      dispatch({
        type: actionType.GET_PRODUCTS_FAILURE,
        error: new Error("Fail to get products"),
      });
    }
  };
}

export function filterProducts(filter) {
  return (dispatch) => {
    dispatch({
      type: actionType.FILTER_PRODUCTS,
      payload: filter,
    });
  };
}
