import React from "react";
import ErrorComponent from "./error";
import thunk from "redux-thunk";
import { render, screen, act } from "@testing-library/react";
import { createStore, applyMiddleware } from "redux";
import { MemoryRouter } from "react-router-dom";
import reducers from "../reducers";
import { Provider } from "react-redux";
import { actionType } from "../actions";

describe("Error", () => {
  it("should render error component", async () => {
    const store = createStore(reducers, applyMiddleware(thunk));
    render(
      <Provider store={store}>
        <MemoryRouter>
          <ErrorComponent />
        </MemoryRouter>
      </Provider>
    );

    const error = new Error("test error");
    await store.dispatch({
      type: actionType.GET_PRODUCTS_FAILURE,
      error,
    });
    expect(screen.queryByText("test error")).toBeTruthy();
  });
});
