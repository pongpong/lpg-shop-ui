import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getProducts, getCategories } from "../actions";
import Pagination from "./pagination";
import SortBy from "./sortby";
import Error from "./error";

const Products = (props) => {
  const {
    productsList,
    allCategories,
    lastQuery,
    getCategories,
    getProducts,
  } = props;

  useEffect(() => {
    getCategories();
    getProducts();
  }, [getCategories, getProducts]);
  if (!productsList) {
    return null;
  }
  const toCategoryName = (allCategories, categoryId) => {
    return allCategories && allCategories[categoryId].name;
  };
  return (
    <div className="col-lg-9">
      <div className="row">
        <div className="col-lg-9">
          <Error />
          <Pagination />
        </div>
        <div className="col-lg-3">
          <SortBy
            onChange={(value) =>
              getProducts({
                ...lastQuery,
                sort: value,
              })
            }
          />
        </div>
      </div>
      <div className="row">
        {productsList.content.map((product) => (
          <div key={product.id} className="col-lg-4 col-md-6 mb-4">
            <div className="card h-100">
              <Link to="#">
                <img
                  className="card-img-top"
                  src="https://via.placeholder.com/70x45"
                  alt={product.name}
                />
              </Link>
              <div className="card-body">
                <h4 className="card-title">
                  <Link to="#">{product.name}</Link>
                </h4>
                <p className="card-text">{product.description}</p>
              </div>
              <div className="card-footer">
                <small className="text-muted">
                  {toCategoryName(allCategories, product.categoryId)}
                </small>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    productsList: state.products,
    allCategories: state.allCategories,
    lastQuery: state.lastQuery,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getProducts: (params) => {
    dispatch(getProducts(params));
  },
  getCategories: () => {
    dispatch(getCategories());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
