import React from "react";
import { useSelector } from "react-redux";
import { connect } from "react-redux";
import Alert from "react-bootstrap/Alert";

const Error = (props) => {
  const error = useSelector((props) => props.error);
  if (!error) return null;
  return <Alert variant="danger">{error.message}</Alert>;
};

const mapStateToProps = (state) => {
  return {
    error: state.error,
  };
};

export default connect(mapStateToProps, null)(Error);
