import React from "react";
import Home from "./home";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reducers from "../reducers";

describe("Home", () => {
  it("should render home component", () => {
    const store = createStore(reducers, applyMiddleware(thunk));
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      </Provider>
    );
    expect(container).toMatchSnapshot();
  });
});
