import React from "react";
import SortBy from "./sortby";
import { MemoryRouter } from "react-router-dom";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reducers from "../reducers";
import { render, screen } from "@testing-library/react";
import { rest, server } from "../mocks/server";

describe("Home", () => {
  it("should render sortby component", async () => {
    const store = createStore(reducers, applyMiddleware(thunk));
    render(
      <Provider store={store}>
        <MemoryRouter>
          <SortBy />
        </MemoryRouter>
      </Provider>
    );

    const options = screen.queryAllByRole("option");
    expect(options).toHaveLength(4);
    expect(options[0].textContent).toMatch("Name (A to Z)");
    expect(options[1].textContent).toMatch("Name (Z to A)");
    expect(options[2].textContent).toMatch("Category (A to Z)");
    expect(options[3].textContent).toMatch("Category (Z to A)");
  });
});
