import { connect } from "react-redux";
import { getProducts } from "../actions";
import React from "react";

const SortBy = (props) => {
  const { value, sortOptions } = props;
  const onChange = (event) => {
    const { onChange } = props;
    onChange && onChange(event.target.value);
  };
  const listItems = sortOptions.map((item) => (
    <option key={item.value} value={item.value}>
      {item.label}
    </option>
  ));
  return (
    <select value={value} onChange={onChange} className="form-control">
      {listItems}
    </select>
  );
};

const mapStateToProps = (state) => ({
  sortOptions: [
    {
      label: "Name (A to Z)",
      value: "name,asc",
    },
    {
      label: "Name (Z to A)",
      value: "name,desc",
    },
    {
      label: "Category (A to Z)",
      value: "category.name,asc",
    },
    {
      label: "Category (Z to A)",
      value: "category.name,desc",
    },
  ],
  value: state.currentSortOption,
});

const mapDispatchToProps = (dispatch) => ({
  getProducts: (params) => {
    dispatch(getProducts(params));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SortBy);
