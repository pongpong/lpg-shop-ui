import React from "react";
import Pagination from "./pagination";
import { render, screen, act } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reducers from "../reducers";
import { rest, server } from "../mocks/server";
import { API_ORIGIN, getProducts } from "../actions";

describe("Pagination", () => {
  it("should render pagination component", async () => {
    server.use(
      rest.get(API_ORIGIN + "/api/products", (req, res, ctx) => {
        return res(
          ctx.json({
            content: [
              {
                id: 1,
                name: "Knife Set",
                description: "A set of knives in all shapes and sizes.",
                categoryId: 1,
              },
            ],
            pageable: {
              sort: {
                sorted: false,
                unsorted: true,
                empty: true,
              },
              offset: 0,
              pageNumber: 0,
              pageSize: 1,
              paged: true,
              unpaged: false,
            },
            last: false,
            totalPages: 34,
            totalElements: 34,
            size: 1,
            number: 0,
            sort: {
              sorted: false,
              unsorted: true,
              empty: true,
            },
            numberOfElements: 1,
            first: true,
            empty: false,
          })
        );
      })
    );
    const store = createStore(reducers, applyMiddleware(thunk));
    render(
      <Provider store={store}>
        <MemoryRouter>
          <Pagination />
        </MemoryRouter>
      </Provider>
    );

    act(() => {
      store.dispatch(getProducts({ page: 0, size: 10 }));
    });
    expect(await screen.findByText("1")).toBeTruthy();
  });
});
