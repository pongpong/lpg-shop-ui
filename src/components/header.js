import React from "react";
import { Link } from "react-router-dom";

const Header = (props) => {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container">
          <Link className="navbar-brand" to="/">
            My Store
          </Link>
        </div>
      </nav>
    </header>
  );
};

export default Header;
