import { connect } from "react-redux";
import { getProducts } from "../actions";
import React from "react";
import { Link } from "react-router-dom";
import Pagination from "react-bootstrap/Pagination";

const PageSize = ({ value, onChange }) => {
  const listItems = [5, 10, 20].map((number) => (
    <option key={number.toString()} value={number}>
      {number}
    </option>
  ));

  const handleChange = (event) => {
    onChange && onChange(event.target.value);
  };

  return (
    <select value={value} onChange={handleChange} className="form-control">
      {listItems}
    </select>
  );
};

const generatePagination = (
  pageNo,
  totalPages,
  pageItemSupplier,
  ellipseSupplier
) => {
  let current = pageNo;
  let last = totalPages;
  let delta = 2;
  let left = current - delta;
  let right = current + delta + 1;
  let range = [];
  let rangeWithDots = [];
  let l;

  for (let i = 1; i <= last; i++) {
    if (i === 1 || i === last || (i >= left && i < right)) {
      range.push(i);
    }
  }

  for (let i of range) {
    if (l) {
      if (i - l === 2) {
        rangeWithDots.push(pageItemSupplier(l + 1));
      } else if (i - l !== 1) {
        rangeWithDots.push(ellipseSupplier(i));
      }
    }
    rangeWithDots.push(pageItemSupplier(i));
    l = i;
  }

  return rangeWithDots;
};

const SimplePagination = (props) => {
  const { page, totalPages, size, onPageChange, onSizeChange } = props;

  const p = generatePagination(
    page + 1,
    totalPages,
    (i) => (
      <Pagination.Item
        key={"i" + i}
        active={i - 1 === page}
        onClick={() => onPageChange(i - 1)}
      >
        {i}
      </Pagination.Item>
    ),
    (i) => <Pagination.Ellipsis key={"e" + i} />
  );
  return (
    <>
      <div className="row">
        <div className="col-10">
          <Pagination>
            <Pagination.First onClick={() => onPageChange(0)} />
            <Pagination.Prev
              onClick={() => onPageChange(Math.max(0, page - 1))}
            />
            {p}
            <Pagination.Next
              onClick={() => onPageChange(Math.min(page + 1, totalPages - 1))}
            />
            <Pagination.Last onClick={() => onPageChange(totalPages - 1)} />
          </Pagination>
        </div>
        <div className="col-2">
          <PageSize value={size} onChange={(size) => onSizeChange(size)} />
        </div>
      </div>
    </>
  );
};

SimplePagination.defaultProps = {
  page: 1,
  size: 5,
  totalPages: 1,
  onPageChange: () => void 0,
  onSizeChange: () => void 0,
};

const ProductPagination = (props) => {
  const { pageable, getProducts, lastQuery } = props;
  if (!pageable || !pageable.content.length) {
    return null;
  }
  return (
    <SimplePagination
      onPageChange={(i) =>
        getProducts({
          ...lastQuery,
          page: i,
        })
      }
      onSizeChange={(size) =>
        getProducts({
          ...lastQuery,
          size,
        })
      }
      page={pageable.number}
      size={pageable.size}
      totalPages={pageable.totalPages}
    />
  );
};

const mapStateToProps = (state) => ({
  pageable: state.products,
  lastQuery: state.lastQuery,
});

const mapDispatchToProps = (dispatch) => ({
  getProducts: (params) => {
    dispatch(getProducts(params));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductPagination);
