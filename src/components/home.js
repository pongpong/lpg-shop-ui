import React from "react";
import Products from "./products";

const Home = (props) => {
  return (
    <div className="row home">
      <Products />
    </div>
  );
};

export default Home;
